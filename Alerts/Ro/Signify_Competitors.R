library(tools)
library(mailR)


library(tidyverse)
library(tidytext)
library(lubridate)
library(jsonlite)
library(twitterFunctions)
library(gpgFunctions)
library(googlesheets)
library(rtweet)
library(knitr)
library(twitteR)
library(googlesheets)

gs_auth(token = "/home/gpgAnalytics/Alerts/server/Alerts/Signify_Health/googlesheets_token.rds") #Interactive Authaurization for Google Sheets

bad_outlets <- c("ofboar", "fnwir", "atpham", "sudtri", "asxtex", "classar")
bad_titles <- c("NYC Daybook")

my_start <- as.character(format(Sys.Date() - 7, "%m/%d/%Y"))
my_end <- as.character(format(Sys.Date(), "%m/%d/%Y"))
articles_full <- data.frame()
##%######################################################%##
#                                                          #
####                    LANDMARK HEALTH                 ####
#                                                          #
##%######################################################%##

landmark <- news_new("'Landmark Health' not 'of landmark health'", time_range = "w")

if (!is.null(landmark)){
  articles <- landmark
  
  articles$false_positive <- grepl("landmark health", articles$Snippet)
  articles <- articles %>% 
    filter(false_positive == F) %>% 
    select(-false_positive)
  
  articles$false_positive2 <- grepl("Landmark Health", articles$Snippet)
  articles <- articles %>% 
    filter(false_positive2 == T) %>% 
    select(-false_positive2)
  
  articles$Snippet <- as.character(articles$Snippet)
  articles$Company <- "Landmark"
  articles_full <- rbind(articles_full, articles)
  Sys.sleep(10)
} 
    


##%######################################################%##
#                                                          #
####                    ConcertoHealth                  ####
#                                                          #
##%######################################################%##

concerto <- news_new("ConcertoHealth", time_range = "w")

if (!is.null(concerto)){
  articles <- concerto
  
  articles$Snippet <- as.character(articles$Snippet)
  articles$Company <- "ConcertoHealth"
  articles_full <- rbind(articles_full, articles)
  Sys.sleep(10)
} 

##%######################################################%##
#                                                          #
####                    NaviHealth                      ####
#                                                          #
##%######################################################%##

navi <- news_new("NaviHealth", time_range = "w")

if (!is.null(navi)){
  articles <- navi
  
  articles$Snippet <- as.character(articles$Snippet)
  articles$Company <- "NaviHealth"
  articles_full <- rbind(articles_full, articles)
  Sys.sleep(10)
} 

##%######################################################%##
#                                                          #
####                    Privia Health                   ####
#                                                          #
##%######################################################%##

privia <- news_new('"Privia Health"', time_range = "w")

if (!is.null(privia)){
  articles <- privia
  
  articles$Snippet <- as.character(articles$Snippet)
  articles$Company <- "Privia"
  articles_full <- rbind(articles_full, articles)
  Sys.sleep(10)
} 

##%######################################################%##
#                                                          #
####                    Unite Us                        ####
#                                                          #
##%######################################################%##

unite_us <- news_new('"Unite Us"', time_range = "w")

if (!is.null(unite_us)){
  articles <- unite_us
  articles$false_positive <- grepl("Unite Us", articles$Snippet)
  articles <- articles %>% 
    filter(false_positive == T) %>% 
    select(-false_positive)
  
  articles$Snippet <- as.character(articles$Snippet)
  articles$Company <- "Unite Us"
  articles_full <- rbind(articles_full, articles)
  Sys.sleep(10)
}

##%######################################################%##
#                                                          #
####                    Healthify                       ####
#                                                          #
##%######################################################%##
healthify <- news_new('Healthify', time_range = "w")

if (!is.null(healthify)){
  articles <- healthify
  
  articles$Snippet <- as.character(articles$Snippet)
  articles$Company <- "Healthify"
  articles_full <- rbind(articles_full, articles)
  Sys.sleep(10)
}

##%######################################################%##
#                                                          #
####                    Solera                          ####
#                                                          #
##%######################################################%##
solera <- news_new('Solera Health', time_range = "w")

if (!is.null(solera)){
  articles <- solera
  
  articles$Snippet <- as.character(articles$Snippet)
  articles$Company <- "Solera"
  articles_full <- rbind(articles_full, articles)
  Sys.sleep(10)
}


##%######################################################%##
#                                                          #
####                    Aunt Bertha                     ####
#                                                          #
##%######################################################%##
aunt_bertha <- news_new('"Aunt Bertha"', time_range = "w")

if (!is.null(aunt_bertha)){
  articles <- aunt_bertha
  
  articles$false_positive <- grepl("Aunt Bertha", articles$Snippet)
  articles <- articles %>% 
    filter(false_positive == T) %>% 
    select(-false_positive)
  
  articles$Snippet <- as.character(articles$Snippet)
  if (nrow(articles) > 0){
    articles$Company <- "Aunt Bertha"
    articles_full <- rbind(articles_full, articles)
  }
  
  Sys.sleep(10)
}

##%######################################################%##
#                                                          #
####                    NowPow                          ####
#                                                          #
##%######################################################%##
nowpow <- news_new('NowPow', time_range = "w")

if (!is.null(nowpow)){
  articles <- nowpow
  
  articles$Snippet <- as.character(articles$Snippet)
  articles$Company <- "NowPow"
  articles_full <- rbind(articles_full, articles)
  Sys.sleep(10)
}

##%######################################################%##
#                                                          #
####                    CityBlock                       ####
#                                                          #
##%######################################################%##
cityblock <- news_new('Cityblock health', time_range = "w")

if (!is.null(cityblock)){
  articles <- cityblock
  
  articles$Snippet <- as.character(articles$Snippet)
  articles$Company <- "CityBlock"
  articles_full <- rbind(articles_full, articles)
}



articles_full$Include <- ""
articles_full <- articles_full %>%
  select(Include, Title, Source,Time, Snippet, Company, URL)

competitors_ss <- gs_title("SH Competitors")

competitors_ss <- competitors_ss %>% 
  gs_ws_new(ws_title = paste0("competitors_",Sys.Date()), input = articles_full)

