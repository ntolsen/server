library(gpgFunctions)
library(dplyr)
library(googlesheets)
library(twitterFunctions)

library(mailR)

#SETUP
query <- paste0('("Surprise Billing" OR "surprise bills" OR "surprise bill" OR "surprise medical bills" OR "surprise medical bill" OR',
                ' "surprise medical billing" OR "#surprisemedicalbills" OR "suprise #medicalbills" OR @NoSurpriseBills OR "Doctor Patient Unity"',
                ' OR "TeamHealth" OR "Envision Healthcare" OR @TeamHealth OR @EnvisionLeads OR "Coalition Against Surprise Medical Billing" OR',
                ' "Physicians for Fair Coverage" OR "Out of the Middle" OR "Global Medical Response") OR (("Surprise Billing" OR',
                ' "surprise bills" OR "surprise bill" OR "surprise medical bills" OR "surprise medical bill" OR "surprise medical billing" OR',
                ' "#surprisemedicalbills" OR "suprise #medicalbills" OR @NoSurpriseBills) AND ("Private equity" OR "investors" OR "Wall Street"',
                ' OR "dark money" OR "physician-staffing companies" OR "physician staffing" OR "corporate medicine"))')

query2 <- paste0('(("Surprise Billing" OR "surprise bills" OR "surprise bill" OR "surprise medical bills" OR "surprise medical bill" OR',
                ' "surprise medical billing" OR "#surprisemedicalbills" OR "suprise #medicalbills" OR @NoSurpriseBills OR "Doctor Patient Unity"',
                ' OR "TeamHealth" OR "Envision Healthcare" OR @TeamHealth OR @EnvisionLeads OR "Coalition Against Surprise Medical Billing" OR',
                ' "Physicians for Fair Coverage" OR "Out of the Middle" OR "Global Medical Response") OR (("Surprise Billing" OR',
                ' "surprise bills" OR "surprise bill" OR "surprise medical bills" OR "surprise medical bill" OR "surprise medical billing" OR',
                ' "#surprisemedicalbills" OR "suprise #medicalbills" OR @NoSurpriseBills) ("Private equity" OR "investors" OR "Wall Street"',
                ' OR "dark money" OR "physician-staffing companies" OR "physician staffing" OR "corporate medicine"))) is:verified')


START <- paste0(format(Sys.Date()-1,"%Y%m%d"), "1200")
END <- paste0(format(Sys.Date(),"%Y%m%d"), gsub(":","",str_extract(Sys.time(), ".{2}:.{2}")))
gnipUser <- "nolsen@gpg.com"
gnipPass <- "WAv&8&Y%+^{?x7jC8{.y"
gs_auth(token = "/home/gpgAnalytics/Alerts/server/Alerts/Signify_Health/googlesheets_token.rds") #Interactive Authaurization for Google Sheets

sources_sheet <- gs_title("Flag Words")
known_sources <- gs_read(ss=sources_sheet, ws = "Google Outlets")
known_list <- known_sources$Domain


#News
news <- news_new(query, time_range = "d")

news <- news %>% 
  mutate_all(as.character)

new_news <- news %>% 
  mutate(Domain = gsub("/.*", "",gsub("http[s]?://","",URL))) %>% 
  filter(!(Domain %in% known_list)) %>% 
  select(Domain) %>% 
  mutate(Primary = "",
         Industry = "") %>% 
  dplyr::distinct(.keep_all = T)

if (nrow(new_news) > 0){
  gs_add_row(sources_sheet, ws = "Google Outlets", input = new_news, verbose = TRUE)
}

#Twitter
tweets <- gnip_search(query2, n = 1000, START, END, includeRT = F, gnipUser, gnipPass)
tweets_copy <- tweets
tweets <- tweets_copy
#News HTML
content_news <- ''

if (nrow(news) > 0){
  for (i in 1:nrow(news)){
    
    title <- news$Title[i]
    source <- news$Source[i]
    snippet <- news$Snippet[i]
    date <- news$Time[i]
    url <- news$URL[i]
    
    title_url <- paste0('<div class="newsbox"><a href="',url,'">',title,"</a>")
    
    content_news <- paste0(content_news,
                           "<b>", 
                           title_url, 
                           "</b><br/>",
                           source
    )
    
    content_news <- paste0(content_news,"<br/>",date,
                           "<br><br>",
                           "<i>", snippet, "</i>")
    
    content_news <- paste0(content_news, "</div><br/><br/>")
    
    
  } 
  content_full_news <- paste0("<h2>News</h2>", content_news)
  
} else {
  content_news <- "<i>No articles found!<i>"
}
#Twitter HTML
twitter_content <- ''
#BRAND
if (nrow(tweets) > 0){
  trimws(gsub("[^\x01-\x7F]", "", tweets$user.name))
  tweets <- tweets %>% 
    filter(user.verified == T) %>%
    dplyr::arrange(desc(created_at)) %>% 
    select(created_at, id_str, user.screen_name, text, user.profile_image_url, user.name, retweet_count, favorite_count) %>% 
    mutate(tweet_link = paste0('<a href="https://twitter.com/', user.screen_name, "/status/", id_str,'">',user.name,"</a>"))
  for (i in 1:nrow(tweets)){
    twitter_content <- paste0(twitter_content,
                              '<div class ="mybox"><div class="viewport">',
                              '<img src="',tweets$user.profile_image_url[i],'">','<span>',tweets$tweet_link[i], "<i>", " @",
                              tweets$user.screen_name[i], "<br>",tweets$created_at[i],"</i></span></div>",
                              tweets$text[i],"<br/>",
                              '<span class="glyphicon">&#xe115;</span>  ',tweets$retweet_count[i],
                              '&nbsp;<span class="glyphicon">&#9829;</span>  ',tweets$favorite_count[i],
                              "</div><br><br><br>")
  }
} else {
  twitter_content <- "<i>No tweets found!<i>"
}

twitter_content_full <- paste0("<h2>Tweets</h2>", twitter_content)

content_full <- paste0(content_full_news, twitter_content_full)

part_one <- readLines("/home/gpgAnalytics/Alerts/server/Alerts/Remedy/html_one.txt")
part_two <- readLines("/home/gpgAnalytics/Alerts/server/Alerts/Remedy/html_two.txt")


html_one <- ""

for (i in part_one){
  html_one <- paste0(html_one, i)
}



html_two <- ""

for (i in part_two){
  html_two <- paste0(html_two, i)
}


from_email <- "Analytics_Alerts@gloverparkgroup.onmicrosoft.com"

smtp_details <- list(host.name = "smtp.office365.com", port = 587, 
                     user.name = "Analytics_Alerts@gloverparkgroup.onmicrosoft.com", 
                     passwd = "gpgAnalytics!", tls = TRUE)

if (exists("content_full")){
  html_full <- paste0(html_one, content_full ,html_two)
  
  send.mail(from = from_email,
            to = c("nolsen@gpg.com", "dpandove@gpg.com"),
            subject = "TEST CASMB Alert",
            body = html_full,
            authenticate  = TRUE,
            smtp = smtp_details,
            send = TRUE,
            encoding = 'utf-8',
            debug = TRUE,
            html = T)
}
