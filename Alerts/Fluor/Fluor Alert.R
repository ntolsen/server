library(gpgFunctions)
library(dplyr)
library(googlesheets)
library(twitterFunctions)
library(mailR)

#SETUP
query <- '"Jim Bernhard" OR "James Bernhard"'

START <- paste0(format(Sys.Date()-1,"%Y%m%d"), "1200")
END <- paste0(format(Sys.Date(),"%Y%m%d"), gsub(":","",str_extract(Sys.time(), ".{2}:.{2}")))
gnipUser <- "nolsen@gpg.com"
gnipPass <- "WAv&8&Y%+^{?x7jC8{.y"
gs_auth(token = "/home/gpgAnalytics/Alerts/server/Alerts/Signify_Health/googlesheets_token.rds") #Interactive Authaurization for Google Sheets

sources_sheet <- gs_title("Flag Words")
known_sources <- gs_read(ss=sources_sheet, ws = "Google Outlets")
known_list <- known_sources$Domain


#News
f_news <- news_new(query, time_range = "d")

f_news <- f_news %>% 
  mutate_all(as.character)

new_news <- f_news %>% 
  mutate(Domain = gsub("/.*", "",gsub("http[s]?://","",URL))) %>% 
  filter(!(Domain %in% known_list)) %>% 
  select(Domain) %>% 
  mutate(Primary = "",
         Industry = "",
         Name = "",
         City = "",
         State = "",
         Country = "",
         Partisan = "") %>% 
  dplyr::distinct(.keep_all = T)

if (nrow(new_news) > 0){
  gs_add_row(sources_sheet, ws = "Google Outlets", input = new_news, verbose = TRUE)
}


#Twitter
f_tweets <- gnip_search(query, n = 1000, START, END, includeRT = F, gnipUser, gnipPass)

#News HTML
content_news <- ''

if (nrow(f_news) > 0){
    for (i in 1:nrow(f_news)){
      
      title <- f_news$Title[i]
      source <- f_news$Source[i]
      snippet <- f_news$Snippet[i]
      date <- f_news$Time[i]
      url <- f_news$URL[i]
      
      title_url <- paste0('<div class="newsbox"><a href="',url,'">',title,"</a>")
      
      content_news <- paste0(content_news,
                              "<b>", 
                              title_url, 
                              "</b><br/>",
                              source
      )
      
      content_news <- paste0(content_news,"<br/>",date,
                              "<br><br>",
                              "<i>", snippet, "</i>")
      
      content_news <- paste0(content_news, "</div><br/><br/>")
      
      
    } 
    content_full_news <- paste0("<h2>News</h2>", content_news)
  
} else {
  content_news <- "<i>No articles found!<i>"
}
#Twitter HTML
twitter_content <- ''
#BRAND
if (nrow(f_tweets) > 0){
  trimws(gsub("[^\x01-\x7F]", "", f_tweets$user.name))
  f_tweets <- f_tweets %>% 
    select(created_at, id_str, user.screen_name, text, user.profile_image_url, user.name, retweet_count, favorite_count) %>% 
    mutate(tweet_link = paste0('<a href="https://twitter.com/', user.screen_name, "/status/", id_str,'">',user.name,"</a>"))
  for (i in 1:nrow(f_tweets)){
    twitter_content <- paste0(twitter_content,
                                    '<div class ="mybox"><div class="viewport">',
                                    '<img src="',f_tweets$user.profile_image_url[i],'">','<span>',f_tweets$tweet_link[i], "<i>", " @",
                                    f_tweets$user.screen_name[i], "<br>",f_tweets$created_at[i],"</i></span></div>",
                                    f_tweets$text[i],"<br/>",
                                    '<span class="glyphicon">&#xe115;</span>  ',f_tweets$retweet_count[i],
                                    '&nbsp;<span class="glyphicon">&#9829;</span>  ',f_tweets$favorite_count[i],
                                    "</div><br><br><br>")
  }
} else {
  twitter_content <- "<i>No tweets found!<i>"
}

twitter_content_full <- paste0("<h2>Tweets</h2>", twitter_content)

content_full <- paste0(content_full_news, twitter_content_full)

part_one <- readLines("/home/gpgAnalytics/Alerts/server/Alerts/Remedy/html_one.txt")
part_two <- readLines("/home/gpgAnalytics/Alerts/server/Alerts/Remedy/html_two.txt")


html_one <- ""

for (i in part_one){
  html_one <- paste0(html_one, i)
}



html_two <- ""

for (i in part_two){
  html_two <- paste0(html_two, i)
}


from_email <- "Analytics_Alerts@gloverparkgroup.onmicrosoft.com"

smtp_details <- list(host.name = "smtp.office365.com", port = 587, 
                     user.name = "Analytics_Alerts@gloverparkgroup.onmicrosoft.com", 
                     passwd = "gpgAnalytics!", tls = TRUE)

if (exists("content_full")){
  html_full <- paste0(html_one, content_full ,html_two)
  
  send.mail(from = from_email,
            to = c("kfogarty@gpg.com", "cransom@gpg.com", "ekaplan@gpg.com", "nsutter@gpg.com", 
                   "ecarter@gpg.com", "andrew@nealecreek.com", "aking@gpg.com"),
            cc = c("nolsen@gpg.com", "jtaylor@gpg.com"),
            subject = "Fluor Alert",
            body = html_full,
            authenticate  = TRUE,
            smtp = smtp_details,
            send = TRUE,
            encoding = 'utf-8',
            debug = TRUE,
            html = T)
}
