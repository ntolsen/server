#Dupont Alerts
#Library
suppressMessages(library(gpgFunctions))
suppressMessages(library(twitterFunctions))
suppressMessages(library(tidyverse))
suppressMessages(library(mailR))
suppressMessages(library(plyr))
suppressMessages(library(googlesheets))
suppressMessages(library(rtweet))
suppressMessages(library(ggplot2))
suppressMessages(library(plotly))
suppressMessages(library(htmlwidgets))
suppressMessages(library(reshape2))
suppressMessages(library(bigQueryR))
suppressMessages(library(dplyr))
suppressMessages(library(ggthemes))
from_email <- "Analytics_Alerts@gloverparkgroup.onmicrosoft.com"
to_emails <- c('jtaylor@gpg.com', 'nolsen@gpg.com', 'dmintz@gpg.com', 'carmstrong@gpg.com', 'dpandove@gpg.com', 'imoskowitz@gpg.com', 'ecarter@gpg.com', 'ekaplan@gpg.com', 'jminer@gpg.com')
smtp_details <- list(host.name = "smtp.office365.com", port = 587, 
                     user.name = "Analytics_Alerts@gloverparkgroup.onmicrosoft.com", 
                     passwd = "wutang19!", tls = TRUE)
#Authentication
#Twitter
consumer_key <- "J2jhU1QWFVTlGkFfy1A4FF2qL"
consumer_secret <- "Le3qfRPrcfcktfNbKTSbIUefXHCWP2LJrMsGS1FXiE2PFbDI76"
access_token <- "3599847382-HQERjAHL2ZrlI7TN8kOZSi0hCWkK9geplLyhQ9y"
access_secret <- "bErJUDxp0IXM20OlMwXCM9Ft1Xt6HWo3s50R8GhWRqtkv"
#setup_twitter_oauth(consumer_key, consumer_secret, access_token, access_secret)
myapp <- oauth_app("GPG Social Research", key=consumer_key, secret=consumer_secret)
sig <- sign_oauth1.0(myapp, token = access_token, token_secret = access_secret)

#Google Sheets
#To create token
#token <- gs_auth(cache = FALSE)
#gd_token()
#saveRDS(token, file = "/home/noah/Projects/Alerts/DuPont/googlesheets_token.rds")
#To Reference Token
gs_auth(token = "/home/gpgAnalytics/Alerts/server/Alerts/DuPont/googlesheets_token.rds")
## and you're back in business, using the same old token
## if you want silence re: token loading, use this instead
suppressMessages(gs_auth(token = "/home/gpgAnalytics/Alerts/server/Alerts/DuPont/googlesheets_token.rds", verbose = FALSE))


#Pulling The Tweets
handles <- c('fastlerner', 
             'yourturnoutgear', 
             'EWG', 
             'CleanCapeFear', 
             'TalkingPFAS', 
             'theintercept ', 
             'MariahCBlake ', 
             'CNN', 
             'MonicaAmarelo', 
             'sirenamel', 
             'pfoaprojectny1 ', 
             'MarkRuffalo', 
             'StopGenXWilm', 
             'NancySuzyq', 
             'pfasprojectnh', 
             'faberfamilyfarm', 
             'nywaterproject', 
             'WendyENThomas', 
             'melaniebenesh', 
             'rebecca_altman', 
             'edonovan01', 
             'Support4Mindi', 
             'typeinvestigate', 
             'Mike_Schade ', 
             'RedactedTonight', 
             'aropeik', 
             'AlexFormuzis', 
             'SierraClubLive', 
             'MaryGrant_Water', 
             'gennareed', 
             'ECHOActionTeam', 
             'PreventHarm', 
             'LeeCamp', 
             'RepDanKildee', 
             'GarretEllison', 
             'ewgprez', 
             'CHogue ', 
             'WaterTechAll_SD', 
             'LisasOrg', 
             'NewsFallon', 
             'IsmaWati64', 
             'enckj', 
             'saferstates', 
             'PatRizzuto ', 
             'SylviaCarignan', 
             'LaurelSchaider', 
             'JoshmCarney', 
             'CLF', 
             'TiffanyStecker', 
             'CallieJLyons', 
             'NCPolicyWatch', 
             'RepHarley', 
             'WeAct4ej', 
             'ColinONeil', 
             'ewarren', 
             'SenSchumer', 
             'DanaNessel', 
             'LizAGMoran', 
             'LauraRabinow', 
             'CLFMoses ', 
             'greenwatchdogny', 
             'SenCapito', 
             'NidhiSubs ', 
             'RepDebDingell', 
             'EnvirHealthNews', 
             'SilentSpringIns', 
             'ScienceNews', 
             'RobertKennedyJr', 
             'Davabel', 
             'ToxFreeFuture ', 
             'FoodandWater ', 
             'DanShapley', 
             'NYSDEC', 
             'OurHealthFuture', 
             'Environment', 
             'NCDEQ', 
             'Coastal_Review ', 
             'LCVoters ', 
             'SETAC ', 
             'DevilWeKnowFilm', 
             'ORMorrison', 
             'UCSUSA', 
             'SEJORG', 
             'CEH_East', 
             'drdavidmichaels', 
             'ConservationPA', 
             'CleanWaterCT', 
             'MarchForScience', 
             'WaterOnline', 
             'EmilyhHolden', 
             'M_Jfrench ', 
             'j_g_allen', 
             'AlexCKaufman', 
             'Grist', 
             'SaferChemicals ', 
             'DavidBSchultz', 
             'JoeFassler', 
             'ErinBrockovich', 
             'ZoeSchlanger', 
             'RepDean', 
             'RyanFelton', 
             'byIanJames ', 
             'AGBecerra', 
             'ChelseaBrentzel', 
             'ClimateProgress', 
             'NIEHS', 
             'KendraWrites ', 
             'HighWireTalk', 
             'NRDC', 
             'AnnElizabeth18', 
             'PaulaGardner', 
             'JHCannon5', 
             'deb_cohen', 
             'VanBadham', 
             'DZimmerNews', 
             'Alex__Ebert ', 
             'LessCancer', 
             'DeadlineNow', 
             'ChemTrust', 
             'CemShweta', 
             'NCLCV', 
             'CDCgov ', 
             'Eliz_Gribkoff ', 
             'EnvProtectioNet', 
             'SenCoryGardner', 
             'McDanielJustine', 
             'NHDES', 
             'EricLiptonNYT', 
             'BevtheScientist', 
             'SenatorShaheen', 
             'CleanWisconsin', 
             'ElderCongress ', 
             'RepNancyMurphy', 
             'ssteingraber1', 
             'MurphinDC', 
             'CenMag', 
             'EPAresearch', 
             'LenaiJackson', 
             'MotherJones', 
             'CleanHealthyNY', 
             'CPRBlog', 
             'SchwartzNewsNY', 
             'EJForum', 
             'VoteGreenVT ', 
             'Sen_JoeManchin', 
             'VNRCorg', 
             'GaryRuskin', 
             'OrganicConsumer ', 
             'PAlbergo', 
             'lisamurkowski ', 
             'SenDanSullivan', 
             'repdonyoung', 
             'RepGaramendi', 
             'RepHarley', 
             'SenFeinstein', 
             'SenKamalaHarris', 
             'RepDLamborn', 
             'SenatorBennet ', 
             'SenCoryGardner', 
             'SenBlumenthal', 
             'ChrisMurphyCT', 
             'RepJoeCourtney', 
             'rosadelauro', 
             'SenatorCarper', 
             'marcorubio', 
             'KYComer', 
             'RepKClark', 
             'SenMarkey', 
             'RepMcGovern', 
             'RepLoriTrahan', 
             'SenWarren', 
             'RepCummings', 
             'ChrisVanHollen', 
             'SenAngusKing', 
             'RepJackBergman', 
             'RepSlotkin', 
             'RepDebDingell', 
             'RepDanKildee', 
             'RepLawrence', 
             'RepAndyLevin', 
             'RepHaleyStevens', 
             'RashidaTlaib', 
             'RepFredUpton', 
             'RepWalberg', 
             'RepMoolenaar', 
             'SenStabenow', 
             'SenGaryPeters ', 
             'RepRichHudson', 
             'SenatorBurr', 
             'SenThomTillis', 
             'SenatorHassan', 
             'SenatorShaheen', 
             'RepChrisPappas', 
             'FrankPallone', 
             'SenatorMenendez', 
             'CoryBooker', 
             'RepJoshG', 
             'BillPascrell', 
             'AndyKimNJ', 
             'MikieSherrill', 
             'repbenraylujan', 
             'SenatorTomUdall', 
             'MartinHeinrich', 
             'SenSchumer ', 
             'SenGillibrand', 
             'repdelgado', 
             'RepPeteKing ', 
             'RepSeanMaloney', 
             'Jim_Jordan', 
             'RepMikeTurner', 
             'SenSherrodBrown', 
             'senrobportman', 
             'RepSteveStivers', 
             'Inhofepress', 
             'repgregwalden', 
             'SenJeffMerkley', 
             'RepDean', 
             'RepBrendanBoyle', 
             'SenBobCasey', 
             'RepBrianFitz', 
             'SenToomey', 
             'SenJackReed', 
             'MacTXPress', 
             'RepMcEachin', 
             'SenatorWarner ', 
             'timkaine', 
             'SenatorLeahy', 
             'RepAdamSmith', 
             'SenatorCantwell', 
             'PattyMurray', 
             'RepKimSchrier', 
             'RepDennyHeck', 
             'RepRickLarsen', 
             'cathymcmorris', 
             'SenatorBaldwin', 
             'Sen_JoeManchin', 
             'SenCapito ', 
             'RepMcKinley', 
             'SenJohnBarrasso',
             'Participant')
tweets <- data.frame()
for (i in 1:length(handles)) {
  try(tweets_loop <- user_timeline(handles[i], n=100), silent=T)
  try(tweets <- rbind.fill(tweets, tweets_loop), silent=T)
  print(i)
}
tweets$date <- lubridate::as_date(tweets$created_at)
tweets[sapply(tweets , is.list)] <-
  sapply(tweets [sapply(tweets , is.list)],
         function(x)sapply(x, function(y) paste(unlist(y),collapse=", ") ) )
tweets$id_str_original <- tweets$id_str
tweets$id_str <- paste("id:", tweets$id_str, sep="")
tweets$user.screen_name <- tolower(tweets$user.screen_name)
tweets$in_reply_to_screen_name <- tolower(tweets$in_reply_to_screen_name)
tweets$retweet_status <- ifelse(is.na(tweets$retweeted_status.created_at), 'Original', 'Retweet')
tweets$reply_status <- ifelse(is.na(tweets$in_reply_to_screen_name), 'Not Reply', 
                              ifelse(tolower(tweets$in_reply_to_screen_name)==tolower(tweets$user.screen_name), 'Self Reply', 'Reply'))
tweets <- tweets %>%
  dplyr::select(id_str, id_str_original, user.screen_name, retweet_count, favorite_count, retweet_status, 
                text, reply_status, in_reply_to_screen_name, user.description, user.name,
                date, created_at, user.followers_count, user.friends_count, user.favourites_count, user.profile_image_url)
tweets$pull_date <- as.Date(Sys.Date())
tweets$tweet_url <- paste('https://twitter.com/', tweets$user.screen_name, '/status/', tweets$id_str_original , '/', sep="")
tweets$created_at <- as.POSIXct(tweets$created_at)
tweets$created_at <- tweets$created_at + 60*60*4
tz(tweets$created_at) <- "UTC"
tweets <- tweets %>% 
  filter(date == Sys.Date()-1)
names(tweets) <- gsub("[.]", " ", names(tweets))
names(tweets) <- gsub(" ", "_", names(tweets))
#Flag Dictionaries
sheets <- gs_ls()
flag_words_file <- gs_title("Flag Words")
general_flag_original <- unlist(as.list(gs_read(ss=flag_words_file, ws = "Dupont"))) #Pick correct flag words
general_flag_original <- tolower(general_flag_original) 
general_flag <- paste('\\<', general_flag_original, '\\>', sep="", collapse="|")
dupont_flag_original <- 'dupont'
dupont_flag_original <- tolower(dupont_flag_original) 
dupont_flag <- paste('\\<', dupont_flag_original, '\\>', sep="", collapse="|")
#Flagging the tweets
tweets$general_flag <- grepl(general_flag, tweets$text, ignore.case = T)
tweets$dupont_flag <- grepl(dupont_flag, tweets$text, ignore.case = T)
tweets_general <- tweets %>%
  dplyr::filter(general_flag == TRUE)
tweets_dupont <- tweets %>%
  dplyr::filter(dupont_flag == TRUE)
content <- ''
if (length(tweets_general$text) > 0) {
  knitr::opts_chunk$set(error = TRUE)
  tweets_general <- tweets_general %>%
    dplyr::select(date, user_screen_name, user_name, user_description, text, retweet_status, tweet_url, user_profile_image_url)
  urls <- tweets_general$tweet_url
  try(tweets_general$user_name <- paste0('<a href="',urls,'">',tweets_general$user_name, "</a>"), silent=TRUE)
  tweets_general <- tweets_general %>%
    dplyr::select(date, user_name,user_screen_name, user_description, text, retweet_status, user_profile_image_url) %>%
    dplyr::rename('Date' = 'date',
                  'Name' = 'user_name',
                  'Handle' = 'user_screen_name',
                  'Bio' = 'user_description',
                  'Tweet' = 'text',
                  'Retweet or Original' = 'retweet_status')
  
  twitter_general_content <- ""
  for (i in 1:nrow(tweets_general)){
    twitter_general_content <- paste0(twitter_general_content,
                                      '<div class ="mybox"><div class="viewport">',
                                      '<img src="',tweets_general$user_profile_image_url[i],'">','<span>',tweets_general$Name[i], "<i>", " @",
                                      tweets_general$Handle[i], "<br>",tweets_general$Date[i],"</i></span></div>",
                                      tweets_general$Tweet[i], "</div><br><br><br>")
  }
} 
if (length(tweets_dupont$text) > 0) {
  knitr::opts_chunk$set(error = TRUE)
  tweets_dupont <- tweets_dupont %>%
    dplyr::select(date, user_screen_name, user_name, user_description, text, retweet_status, tweet_url, user_profile_image_url)
  urls <- tweets_dupont$tweet_url
  try(tweets_dupont$user_name <- paste0('<a href="',urls,'">',tweets_dupont$user_name, "</a>"), silent=TRUE)
  tweets_dupont <- tweets_dupont %>%
    dplyr::select(date, user_name,user_screen_name, user_description, text, retweet_status, user_profile_image_url) %>%
    dplyr::rename('Date' = 'date',
                  'Name' = 'user_name',
                  'Handle' = 'user_screen_name',
                  'Bio' = 'user_description',
                  'Tweet' = 'text',
                  'Retweet or Original' = 'retweet_status')
  
  twitter_dupont_content <- ""
  for (i in 1:nrow(tweets_dupont)){
    twitter_dupont_content <- paste0(twitter_dupont_content,
                                     '<div class ="mybox"><div class="viewport">',
                                     '<img src="',tweets_dupont$user_profile_image_url[i],'">','<span>',tweets_dupont$Name[i], "<i>", " @",
                                     tweets_dupont$Handle[i], "<br>",tweets_dupont$Date[i],"</i></span></div>",
                                     tweets_dupont$Tweet[i], "</div><br><br><br>")
  }
} else {
  twitter_dupont_content  <- "<i>No tweets found!<i>"
}
#Creating a graph of the summary data
twitter_aggregate_summary <- tweets %>%
  dplyr::group_by(date) %>%
  dplyr::summarise(total_tweets = n(),
                   total_topic_tweets = sum(general_flag == TRUE),
                   total_dupont_tweets = sum(dupont_flag == TRUE))
twitter_average_summary <- tweets %>%
  dplyr::group_by(date, user_screen_name) %>%
  dplyr::summarise(total_tweets = n(),
                   total_topic_tweets = sum(general_flag == TRUE),
                   total_dupont_tweets = sum(dupont_flag == TRUE)) %>%
  dplyr::group_by(date) %>%
  dplyr::summarise(per_user_tweets = mean(total_tweets),
                   per_user_topic_tweets = mean(total_topic_tweets),
                   per_user_dupont_tweets = mean(total_dupont_tweets))
twitter_summary <- merge(twitter_aggregate_summary, twitter_average_summary, by='date')
twitter_summary[2,] <- NA


#Reading in the historic dataset
myClientId <-"607975780203-7f2b9ghtpca7i339kuadhfv4eo3so6gh.apps.googleusercontent.com"
clientSecret <- "OfQB7OGE774e7X90vL-Gh7ZW"

options(googleAuthR.client_id = myClientId,
        googleAuthR.client_secret = clientSecret,
        googleAuthR.webapp.client_id = myClientId,
        googleAuthR.scopes.selected = "https://www.googleapis.com/auth/bigquery")

googleAuthR::gar_auth_service("/home/gpgAnalytics/Alerts/server/Alerts/DuPont/GPG Alerts-c6b501fd19e1.json")

#pulling down the most recent dataset

#Creating the blank table
#bigQueryR::bqr_create_table("gpg-alerts","dupont_tweets", "twitter_summary", twitter_summary)
#Bringing in the old data
twitter_summary_old <- bigQueryR::bqr_query("gpg-alerts",
                                            "dupont_tweets",
                                            "SELECT * FROM twitter_summary")

twitter_summary <- rbind(twitter_summary_old, twitter_summary)
twitter_summary <- twitter_summary[!is.na(twitter_summary$date),]

#Uploading the new data
bigQueryR::bqr_upload_data(projectId = "gpg-alerts",
                           datasetId = "dupont_tweets",
                           tableId ="twitter_summary", 
                           upload_data = twitter_summary,
                           create = c("CREATE_IF_NEEDED"))


#Creating the dataset overtime
twitter_average_summary <- twitter_summary %>%
  dplyr::select(date, per_user_tweets, per_user_topic_tweets, per_user_dupont_tweets) %>%
  dplyr::rename(`Tweets Per User` = per_user_tweets,
                `Topic Tweets Per User` =per_user_topic_tweets, 
                `Dupont Tweets Per User` = per_user_dupont_tweets)
twitter_average_summary <- melt(twitter_average_summary, 'date')
twitter_average_summary <- twitter_average_summary[!is.na(twitter_average_summary$date),]


#Plotting the data up to this point
twitter_average_summary$date <- as.Date(twitter_average_summary$date)
tweets_over_time <- ggplot() + 
  geom_line(data = twitter_average_summary, aes(x = date, y = value, colour = variable), size=2) +
  geom_point() +
  xlab('') +
  ylab('Tweets per User') + 
  scale_colour_manual(values = c('#67338F', '#D02C2B', '#409BDB')) +
  theme_tufte() +
  theme(legend.position="top") +
  #scale_x_date(date_labels = "%d %m %y") +
  theme(text = element_text(size=15))

ggsave("dupont.png")
print(tweets_over_time)
encodeGraphic <- function(g) {
  png(tf1 <- tempfile(fileext = ".png"))  # Get an unused filename in the session's temporary directory, and open that file for .png structured output.
  print(g)  # Output a graphic to the file
  dev.off()  # Close the file.
  txt <- RCurl::base64Encode(readBin(tf1, "raw", file.info(tf1)[1, "size"]), "txt")  # Convert the graphic image to a base 64 encoded string.
  myImage <- htmltools::HTML(sprintf('<img src="data:image/png;base64,%s">', txt))  # Save the image as a markdown-friendly html object.
  return(myImage)
}
hg <- encodeGraphic(tweets_over_time)  # run the function that base64 encodes the graph
hg <- '<img class="graph_image" src="cid:dupont"/>'
hg <- paste('<div class="graph_image">', hg, '</div>')
#hg <- as.character(hg)
#forHTML <- list(h1("My header"), p("Lead-in text about the graph"), hg)
#save_html(hg, 'tweets.html')
content_full_news_daily <- paste0("<h2>Tweet Behavior Over Time</h2>", hg, "<h2>DuPont Content</h2>", twitter_dupont_content)
if(exists("content_full_news_daily")){
  content_full <- content_full_news_daily
} else {
  print(paste("No hits at", Sys.time() - 3600*4))
}
part_one <- readLines("/home/gpgAnalytics/Alerts/server/Alerts/Tencent/html_one.txt")
part_two <- readLines("/home/gpgAnalytics/Alerts/server/Alerts/Tencent/html_two.txt")
html_one <- ""
for (i in part_one){
  html_one <- paste0(html_one, i)
}
html_two <- ""
for (i in part_two){
  html_two <- paste0(html_two, i)
}
if (exists("content_full")){
  html_full <- paste0(html_one, content_full, html_two)
  
  send.mail(from = from_email,
            to = to_emails,
            cc = c("jtaylor@gpg.com"),
            subject = "DuPont Alerts",
            body = html_full,
            authenticate  = TRUE,
            attach.files = "dupont.png",
            file.names = "dupont",
            smtp = smtp_details,
            send = TRUE,
            encoding = 'utf-8',
            debug = TRUE,
            html = T)
}

