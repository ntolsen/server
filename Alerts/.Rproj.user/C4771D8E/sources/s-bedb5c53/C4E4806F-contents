library(gpgFunctions)
library(lubridate)
library(scales)
library(ggthemes)
library(mailR)

##%######################################################%##
#                                                          #
####                      PLOTTING                      ####
#                                                          #
##%######################################################%##


bad_outlets <- c("ofboar", "fnwir", "atpham", "sudtri", "asxtex", "classar", "wc50873")
bad_titles <- c("NYC Daybook")


query <- paste0("((UCLA or 'University of California Los Angeles' or U.C.L.A)",
                " near10 (gynecologist or 'sexual abuse' or 'sexual battery' or",
                " apologizes or MeToo or 'Me Too' or yelp or apologizes)) or(",
                " 'Dr. James Heaps' or 'Dr. James Mason Heaps' or 'James Heaps' or 'Dr. Heaps')")

ucla <- get_articles(query, 
                     startDate = as.character(format(Sys.Date()-1, "%m/%d/%y")), 
                     endDate = as.character(format(Sys.Date(), "%m/%d/%y")))
articles <- ucla$articles

articles <- articles %>% 
  filter(!(SourceCode %in% bad_outlets)) %>% 
  filter(!(Title %in% bad_titles))

articles$Snippet <- as.character(articles$Snippet)
articles$Section <- as.character(articles$Section)
articles$Copyright <- as.character(articles$Copyright)
write_csv(articles, "ucla_articles.csv")
volume_plot <- articles %>% 
  dplyr::group_by(Date = date(PublicationDate)) %>% 
  dplyr::count() %>% 
  ggplot(aes(Date, n)) + geom_line() + ggtitle("Daily Article Volume by Since Story Break", subtitle = paste0("6/10-",gsub('^0','',format(Sys.Date(), "%m/%d")))) +
  ggthemes::theme_tufte() + scale_y_continuous(breaks= pretty_breaks()) + ylab("Articles") + xlab("")

ggsave("/home/noah/Projects/Alerts/UCLA/volume.jpg",volume_plot, device = "jpeg")

outlet_plot <- articles %>% 
  dplyr::group_by(Outlet = SourceName) %>% 
  dplyr::count() %>% 
  ggplot(aes(reorder(Outlet, n), n)) + geom_bar(stat = 'identity') + ggtitle("Articles By Outlet", subtitle = paste0("6/10-",gsub('^0','',format(Sys.Date(), "%m/%d")))) +
  ggthemes::theme_tufte() + coord_flip() + xlab("") + scale_y_continuous(breaks= pretty_breaks())

ggsave("/home/noah/Projects/Alerts/UCLA/outlets.jpg",outlet_plot, device = "jpeg", height = 5)

##%######################################################%##
#                                                          #
####                      ARTICLES                      ####
#                                                          #
##%######################################################%##
ucla <- get_articles(query, 
                     startDate = as.character(format(Sys.Date()-1, "%m/%d/%y")), 
                     endDate = as.character(format(Sys.Date(), "%m/%d/%y")))
articles <- ucla$articles

articles <- articles %>% 
  filter(!(SourceCode %in% bad_outlets)) %>% 
  filter(!(Title %in% bad_titles))

articles$Title <- gsub("<hlt>", "",articles$Title)
articles$Title <- gsub("</hlt>", "", articles$Title)

articles$Snippet <- as.character(articles$Snippet)
articles$Snippet <- gsub("<hlt>", "<b>",articles$Snippet)
articles$Snippet <- gsub("</hlt>", "</b>", articles$Snippet)

articles <- articles %>% 
  arrange(desc(PublicationDate))


content <- ""
if (nrow(articles) > 0){
  
  for (i in 1:nrow(articles)){
    
    title <- articles$Title[i]
    source <- articles$SourceName[i]
    author <- articles$Author[i]
    snippet <- articles$Snippet[i]
    link  <- articles$Link[i]
    date <- articles$PublicationDate[i]
    
    search_Word <- paste(title,source)
    search_Word <- gsub(";", "", search_Word)
    search_Word <- gsub("</hlt>", "", search_Word)
    search_Word <- gsub("[^[:alnum:] ]", "", search_Word)
    
    if (str_detect(link, "redirect") == TRUE){
      url <- httr::GET(link)
      url <- url$all_headers[[1]]$headers$location
      title_url <- paste0('<a href="',url,'">',title,"</a>")
      articles$URL_GUESS[i] <- url
    } else if (str_detect(link, "redirect") == FALSE & str_detect(link, "dowjones")){
      
      try(test <- google_basic(search_Word), silent=T)
      if (!is.na(test)){
        url <- test$URL[1]
        title_url <- paste0('<a href="',url,'">',title,"</a>")
        articles$URL_GUESS[i] <- url
      }
      
    } else {
      title_url <- paste0('<a href="',link,'">',title,"</a>")
      articles$URL_GUESS[i] <- link
    }
    
    content <- paste0(content,'<div class="newsbox">',
                      "<b>", 
                      title_url, 
                      "</b><br/>",
                      source
    )
    
    if (!is.na(author)){
      
      content <- paste0(content,
                        ", ",
                        author)
      
    }
    content <- paste0(content,"<br/>",date,
                      "<br><br>",
                      "<i>", snippet, "</i></div>")
    
    if (i != nrow(articles)){
      content <- paste0(content, "<br/><br/>")
    }
    
  }
  
} else {
  content <- "<i>No articles found.</i>"
}





content_ucla <- paste0('<h2 align="center"><u>UCLA Alerts ',format(Sys.Date()-1, "%B %d, %Y")," - ",format(Sys.Date(), "%B %d, %Y"), '</u></h2>',content)

##%######################################################%##
#                                                          #
####                   EMAIL SECTION                    ####
#                                                          #
##%######################################################%##


test <- paste0("<html>
               <head>
               <title></title>
               <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
               <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
               <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
               <style type=\"text/css\">
               /* FONTS */
               .viewport {
               width: 350px;
               padding: 5px;
               position: relative;
               }
               .mybox, .newsbox {
               box-shadow: 10px 10px 5px #aaaaaa;
               border: 1px solid #BFBFBF;
               padding-bottom: 15px;
               padding-left: 15px;
               padding-right: 15px;
               padding-top: 10px;
               }
               .viewport img {
               position: absolute;
               top: 50%;
               margin-top: -30px;  /* = image height div 2 */
               }
               .viewport span {
               margin-left: 68px;  /* = image width + 8 */
               display: block;    
               }
               
               @media screen {
               @font-face {
               font-family: 'Lato';
               font-style: normal;
               font-weight: 400;
               src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
               }
               @font-face {
               font-family: 'Lato';
               font-style: normal;
               font-weight: 700;
               src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
               }
               @font-face {
               font-family: 'Lato';
               font-style: italic;
               font-weight: 400;
               src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
               }   
               @font-face {
               font-family: 'Lato';
               font-style: italic;
               font-weight: 700;
               src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
               }
               }
               /* CLIENT-SPECIFIC STYLES */
               body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
               table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
               img { -ms-interpolation-mode: bicubic; }
               /* RESET STYLES */
               img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
               table { border-collapse: collapse !important; }
               body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }
               
               /* iOS BLUE LINKS */
               a[x-apple-data-detectors] {
               color: inherit !important;
               text-decoration: none !important;
               font-size: inherit !important;
               font-family: inherit !important;
               font-weight: inherit !important;
               line-height: inherit !important;
               }
               /* MOBILE STYLES */
               @media screen and (max-width:600px){
               h1 {
               font-size: 32px !important;
               line-height: 32px !important;
               }
               }
               /* ANDROID CENTER FIX */
               div[style*=\"margin: 16px 0;\"] { margin: 0 !important; }
               </style>
               </head>
               <body style=\"background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;\">
               <!-- HIDDEN PREHEADER TEXT -->
               <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
               <!-- LOGO -->
               <tr>
               <td bgcolor=\"#e11b30\" align=\"center\">
               <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" >
               <tr>
               <td align=\"center\" valign=\"top\" style=\"padding: 40px 10px 40px 10px;\">
               <a href=\"https://gpg.com/\" target=\"_blank\">
               <img alt=\"Logo\" src=\"https://gpg.com/assets/img/gpg-rv.svg\" width=\"100\" height=\"40\" style=\"display: block; width: 40px; max-width: 40px; min-width: 250px; font-family: 'Lato', Helvetica, Arial, sans-serif; color: #ffffff; font-size: 18px;\" border=\"0\">
               </a>
               </td>
               </tr>
               </table>
               </td>
               </tr>
               <!-- HERO -->
               <tr>
               <td bgcolor=\"#e11b30\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">
               <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" >
               <tr>
               <td bgcolor=\"#ffffff\" align=\"center\" valign=\"top\" style=\"padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;\">
               </td>
               </tr>
               </table>
               </td>
               </tr>
               <!-- COPY BLOCK -->
               <tr>
               <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">
               <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" >
               <!-- COPY -->
               <tr>
               <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 20px 30px 40px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\" >
               <p style=\"margin: 0;\">",content_ucla,"</p>
               </td>
               </tr>
               <!-- BULLETPROOF BUTTON -->
               <tr>
               <td bgcolor=\"#ffffff\" align=\"left\">
               <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
               <tr>
               <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 20px 30px 60px 30px;\">
               </td>
               </tr>
               </table>
               </td>
               </tr>
               </table>
               </td>
               </tr>
               <!-- COPY CALLOUT -->
               <tr>
               <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">
               
               </td>
               </tr>
               <!-- SUPPORT CALLOUT -->
               <tr>
               <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 30px 10px 0px 10px;\">
               <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" >
               <!-- HEADLINE -->
               <tr>
               <td bgcolor=\"#FFE0DE\" align=\"center\" style=\"padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\" >
               <h2 style=\"font-size: 20px; font-weight: 400; color: #111111; margin: 0;\">Questions?</h2>
               <p style=\"margin: 0;\"><a href=\"mailto:analytics@gpg.com?Subject=Analytics%20Alerts\" target=\"_blank\" style=\"color: #ec6d64;\">Email Us</a></p>
               </td>
               </tr>
               </table>
               </td>
               </tr>
               <!-- FOOTER -->
               <tr>
               <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">
               
               </td>
               </tr>
               </table>
               </body>
               </html>")



from_email <- "Analytics_Alerts@gloverparkgroup.onmicrosoft.com"
to_emails <- c("lauren.miller@gpg.com")
smtp_details <- list(host.name = "smtp.office365.com", port = 587, 
                     user.name = "Analytics_Alerts@gloverparkgroup.onmicrosoft.com", 
                     passwd = "AnALYtiCKS-EndGAME", tls = TRUE)

#SLACK EMAIL: "z4p5k3o1a3y0x0k5@gpg.slack.com"

send.mail(from = from_email,
          to = to_emails,
          bcc = "nolsen@gpg.com",
          subject = "UCLA Alerts",
          body = test,
          attach.files = c("/home/noah/Projects/Alerts/UCLA/volume.jpg","/home/noah/Projects/Alerts/UCLA/outlets.jpg"),
          authenticate  = TRUE,
          smtp = smtp_details,
          send = TRUE,
          encoding = 'utf-8',
          debug = TRUE,
          html = T)
