library(tools)
library(mailR)
library(tidyverse)
library(tidytext)
library(lubridate)
library(jsonlite)
library(twitterFunctions)
library(gpgFunctions)
library(googlesheets)

##%######################################################%##
#                                                          #
####                    NEWS SECTION                    ####
#                                                          #
##%######################################################%##

approved_articles <- gs_title("SH_Articles")
approved_articles <- gs_read(ss=approved_articles, ws = paste0("articles_",Sys.Date()))

for_content <- approved_articles %>% 
  filter(Include == "Yes") %>% 
  arrange(desc(PublicationDate))

for_content$Author <- toTitleCase(tolower(for_content$Author))
content <- ""

for (i in 1:nrow(for_content)){
  
  title <- for_content$Title[i]
  source <- for_content$SourceName[i]
  author <- for_content$Author[i]
  snippet <- for_content$Snippet[i]
  date <- for_content$PublicationDate[i]
  url <- for_content$URL_GUESS[i]
  
  title_url <- paste0('<div class="newsbox"><a href="',url,'">',title,"</a>")
  
  content <- paste0(content,
                    "<b>", 
                    title_url, 
                    "</b><br/>",
                    source
  )
  
  if (!is.na(author)){
    
    content <- paste0(content,
                      ", ",
                      author)
    
  }
  content <- paste0(content,"<br/>",date,
                    "<br><br>",
                    "<i>", snippet, "</i>")
  
    content <- paste0(content, "</div><br/><br/>")

  
}

content_full_news <- paste0('<h2 align="center"><u>Signify Health</u></h2>',"<h2>News</h2>", content)
##%######################################################%##
#                                                          #
####                  TWITTER SECTION                   ####
#                                                          #
##%######################################################%##

approved_tweets <- gs_title("SH Tweets")
approved_tweets <- gs_read(ss=approved_tweets, ws = paste0("tweets_",Sys.Date()))

for_content_tweets <- approved_tweets %>% 
  filter(Include == "Yes")

twitter_content <- ""
if (nrow(for_content_tweets) > 0){
  for (i in 1:nrow(for_content_tweets)){
    twitter_content <- paste0(twitter_content,
                              '<div class ="mybox"><div class="viewport">',
                              '<img src="',for_content_tweets$user_profile_image_url[i],'">','<span>',for_content_tweets$Name[i], "<i>", " @",
                              for_content_tweets$Handle[i], "<br>",for_content_tweets$Date[i],"</i></span></div>",
                              for_content_tweets$Tweet[i], "</div><br><br><br>")
  }
}


content_full <- paste0(content_full_news, "<br><h2>Twitter</h2>", twitter_content)

##%######################################################%##
#                                                          #
####                   EMAIL SECTION                    ####
#                                                          #
##%######################################################%##


test <- paste0("<html>
               <head>
               <title></title>
               <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
               <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
               <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
               <style type=\"text/css\">
               /* FONTS */
               .viewport {
               width: 350px;
               padding: 5px;
               position: relative;
               }
               .mybox, .newsbox {
               box-shadow: 10px 10px 5px #aaaaaa;
               border: 1px solid #BFBFBF;
               padding-bottom: 15px;
               padding-left: 15px;
               padding-right: 15px;
               padding-top: 10px;
               }
               .viewport img {
               position: absolute;
               top: 50%;
               margin-top: -30px;  /* = image height div 2 */
               }
               .viewport span {
               margin-left: 68px;  /* = image width + 8 */
               display: block;    
               }
               
               @media screen {
               @font-face {
               font-family: 'Lato';
               font-style: normal;
               font-weight: 400;
               src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
               }
               @font-face {
               font-family: 'Lato';
               font-style: normal;
               font-weight: 700;
               src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
               }
               @font-face {
               font-family: 'Lato';
               font-style: italic;
               font-weight: 400;
               src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
               }   
               @font-face {
               font-family: 'Lato';
               font-style: italic;
               font-weight: 700;
               src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
               }
               }
               /* CLIENT-SPECIFIC STYLES */
               body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
               table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
               img { -ms-interpolation-mode: bicubic; }
               /* RESET STYLES */
               img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
               table { border-collapse: collapse !important; }
               body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }
               
               /* iOS BLUE LINKS */
               a[x-apple-data-detectors] {
               color: inherit !important;
               text-decoration: none !important;
               font-size: inherit !important;
               font-family: inherit !important;
               font-weight: inherit !important;
               line-height: inherit !important;
               }
               /* MOBILE STYLES */
               @media screen and (max-width:600px){
               h1 {
               font-size: 32px !important;
               line-height: 32px !important;
               }
               }
               /* ANDROID CENTER FIX */
               div[style*=\"margin: 16px 0;\"] { margin: 0 !important; }
               </style>
               </head>
               <body style=\"background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;\">
               <!-- HIDDEN PREHEADER TEXT -->
               <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
               <!-- LOGO -->
               <tr>
               <td bgcolor=\"#e11b30\" align=\"center\">
               <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" >
               <tr>
               <td align=\"center\" valign=\"top\" style=\"padding: 40px 10px 40px 10px;\">
               <a href=\"https://gpg.com/\" target=\"_blank\">
               <img alt=\"Logo\" src=\"https://gpg.com/assets/img/gpg-rv.svg\" width=\"100\" height=\"40\" style=\"display: block; width: 40px; max-width: 40px; min-width: 250px; font-family: 'Lato', Helvetica, Arial, sans-serif; color: #ffffff; font-size: 18px;\" border=\"0\">
               </a>
               </td>
               </tr>
               </table>
               </td>
               </tr>
               <!-- HERO -->
               <tr>
               <td bgcolor=\"#e11b30\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">
               <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" >
               <tr>
               <td bgcolor=\"#ffffff\" align=\"center\" valign=\"top\" style=\"padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;\">
               </td>
               </tr>
               </table>
               </td>
               </tr>
               <!-- COPY BLOCK -->
               <tr>
               <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">
               <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" >
               <!-- COPY -->
               <tr>
               <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 20px 30px 40px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\" >
               <p style=\"margin: 0;\">",content_full,"</p>
               </td>
               </tr>
               <!-- BULLETPROOF BUTTON -->
               <tr>
               <td bgcolor=\"#ffffff\" align=\"left\">
               <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
               <tr>
               <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 20px 30px 10px 30px;\">
               </td>
               </tr>
               </table>
               </td>
               </tr>
               </table>
               </td>
               </tr>
               <!-- COPY CALLOUT -->
               <tr>
               <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">
               
               </td>
               </tr>
               <!-- SUPPORT CALLOUT -->
               <tr>
               <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 30px 10px 0px 10px;\">
               <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" >
               <!-- HEADLINE -->
               <tr>
               <td bgcolor=\"#FFE0DE\" align=\"center\" style=\"padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\" >
               <h2 style=\"font-size: 20px; font-weight: 400; color: #111111; margin: 0;\">Questions?</h2>
               <p style=\"margin: 0;\"><a href=\"mailto:egross@gpg.com?Subject=Signify%20Health%20Alerts\" target=\"_blank\" style=\"color: #ec6d64;\">Email Us</a></p>
               </td>
               </tr>
               </table>
               </td>
               </tr>
               <!-- FOOTER -->
               <tr>
               <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">
                                                  
                                                  </td>
                                                  </tr>
                                                  </table>
                                                  </body>
                                                  </html>")


distro_list <- read_csv("/home/noah/Projects/Alerts/Signify_Health/distro_list.csv")
from_email <- "Analytics_Alerts@gloverparkgroup.onmicrosoft.com"
to_emails <- distro_list$Email
smtp_details <- list(host.name = "smtp.office365.com", port = 587, 
                     user.name = "Analytics_Alerts@gloverparkgroup.onmicrosoft.com", 
                     passwd = "AnALYtiCKS-EndGAME", tls = TRUE)

#SLACK EMAIL: "z4p5k3o1a3y0x0k5@gpg.slack.com"

send.mail(from = from_email,
          to = distro_list$Email,
          cc = c("egross@gpg.com","kackerman@gpg.com"),
          bcc = c("nolsen@gpg.com", "jtaylor@gpg.com"),
          subject = "Signify Health Alert",
          body = test,
          authenticate  = TRUE,
          smtp = smtp_details,
          send = TRUE,
          encoding = 'utf-8',
          debug = TRUE,
          html = T)


##%######################################################%##
#                                                          #
####                   COMPETITORS EMAIL                ####
#                                                          #
##%######################################################%##

companies_ccm <- c("Landmark", "ConcertoHealth", "NaviHealth", "Privia", "CityBlock")
companies_sdoh <- c("Unite Us", "Healthify", "Solera", "Aunt Bertha", "NowPow")

approved_competitors <- gs_title("SH Competitors")
approved_competitors <- gs_read(ss=approved_competitors, ws = paste0("competitors_",Sys.Date()))

approved_competitors <- approved_competitors %>% 
  filter(Include == "Yes")

content_ccm <- '<h1 align="center"><u>CCM</u></h1>'
for (i in companies_ccm){
  content <- ""
  articles <- approved_competitors %>% 
    filter(Company == i)
  if (nrow(articles) > 0){
    for (j in 1:nrow(articles)){
      title <- articles$Title[j]
      source <- articles$SourceName[j]
      author <- articles$Author[j]
      snippet <- articles$Snippet[j]
      link  <- articles$URL_GUESS[j]
      date <- articles$PublicationDate[j]
      
      title_url <- paste0('<a href="',link,'">',title,"</a>")
      content <- paste0(content,'<div class="newsbox">',
                        "<b>", 
                        title_url, 
                        "</b><br/>",
                        source
      )
      if (!is.na(author)){
        
        content <- paste0(content,
                          ", ",
                          author)
        
      }
      content <- paste0(content,"<br/>",date,
                        "<br><br>",
                        "<i>", snippet, "</i></div>", "<br/><br/>")
      
      
    }
    content_ccm <- paste0(content_ccm, '<h2>',i,'</h2>',content)
  } else {
    content_ccm <- paste0(content_ccm, '<h2>',i,'</h2>', "<br/><br/>")
  }
  
  
}

content_sdoh <- '<h1 align="center"><u>SDOH</u></h1>'

for (i in companies_sdoh){
  content <- ''
  articles <- approved_competitors %>% 
    filter(Company == i)
  if (nrow(articles) > 0){
    for (j in 1:nrow(articles)){
      title <- articles$Title[j]
      source <- articles$SourceName[j]
      author <- articles$Author[j]
      snippet <- articles$Snippet[j]
      link  <- articles$URL_GUESS[j]
      date <- articles$PublicationDate[j]
      
      title_url <- paste0('<a href="',link,'">',title,"</a>")
      content <- paste0(content,'<div class="newsbox">',
                        "<b>", 
                        title_url, 
                        "</b><br/>",
                        source
      )
      if (!is.na(author)){
        
        content <- paste0(content,
                          ", ",
                          author)
        
      }
      content <- paste0(content,"<br/>",date,
                        "<br><br>",
                        "<i>", snippet, "</i></div>", "<br/><br/>")
     
    }
    content_sdoh <- paste0(content_sdoh, '<h2>',i,'</h2>',content)
  } else {
    content_sdoh <- paste0(content_sdoh, '<h2>',i,'</h2>', "<br/><br/>")
    
  }
  
  
}

competitors_content <- paste0(content_ccm, content_sdoh)
competitors_email <- paste0("<html>
               <head>
                            <title></title>
                            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
                            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
                            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
                            <style type=\"text/css\">
                            /* FONTS */
                            .viewport {
                            width: 350px;
                            padding: 5px;
                            position: relative;
                            }
                            .mybox, .newsbox {
                            box-shadow: 10px 10px 5px #aaaaaa;
                            border: 1px solid #BFBFBF;
                            padding-bottom: 15px;
                            padding-left: 15px;
                            padding-right: 15px;
                            padding-top: 10px;
                            }
                            .viewport img {
                            position: absolute;
                            top: 50%;
                            margin-top: -30px;  /* = image height div 2 */
                            }
                            .viewport span {
                            margin-left: 68px;  /* = image width + 8 */
                            display: block;    
                            }
                            
                            @media screen {
                            @font-face {
                            font-family: 'Lato';
                            font-style: normal;
                            font-weight: 400;
                            src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
                            }
                            @font-face {
                            font-family: 'Lato';
                            font-style: normal;
                            font-weight: 700;
                            src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
                            }
                            @font-face {
                            font-family: 'Lato';
                            font-style: italic;
                            font-weight: 400;
                            src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
                            }   
                            @font-face {
                            font-family: 'Lato';
                            font-style: italic;
                            font-weight: 700;
                            src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
                            }
                            }
                            /* CLIENT-SPECIFIC STYLES */
                            body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                            table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                            img { -ms-interpolation-mode: bicubic; }
                            /* RESET STYLES */
                            img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
                            table { border-collapse: collapse !important; }
                            body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }
                            
                            /* iOS BLUE LINKS */
                            a[x-apple-data-detectors] {
                            color: inherit !important;
                            text-decoration: none !important;
                            font-size: inherit !important;
                            font-family: inherit !important;
                            font-weight: inherit !important;
                            line-height: inherit !important;
                            }
                            /* MOBILE STYLES */
                            @media screen and (max-width:600px){
                            h1 {
                            font-size: 32px !important;
                            line-height: 32px !important;
                            }
                            }
                            /* ANDROID CENTER FIX */
                            div[style*=\"margin: 16px 0;\"] { margin: 0 !important; }
                            </style>
                            </head>
                            <body style=\"background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;\">
                            <!-- HIDDEN PREHEADER TEXT -->
                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                            <!-- LOGO -->
                            <tr>
                            <td bgcolor=\"#e11b30\" align=\"center\">
                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" >
                            <tr>
                            <td align=\"center\" valign=\"top\" style=\"padding: 40px 10px 40px 10px;\">
                            <a href=\"https://gpg.com/\" target=\"_blank\">
                            <img alt=\"Logo\" src=\"https://gpg.com/assets/img/gpg-rv.svg\" width=\"100\" height=\"40\" style=\"display: block; width: 40px; max-width: 40px; min-width: 250px; font-family: 'Lato', Helvetica, Arial, sans-serif; color: #ffffff; font-size: 18px;\" border=\"0\">
                            </a>
                            </td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                            <!-- HERO -->
                            <tr>
                            <td bgcolor=\"#e11b30\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">
                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" >
                            <tr>
                            <td bgcolor=\"#ffffff\" align=\"center\" valign=\"top\" style=\"padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;\">
                            </td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                            <!-- COPY BLOCK -->
                            <tr>
                            <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">
                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" >
                            <!-- COPY -->
                            <tr>
                            <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 20px 30px 40px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\" >
                            <p style=\"margin: 0;\">",competitors_content,"</p>
                            </td>
                            </tr>
                            <!-- BULLETPROOF BUTTON -->
                            <tr>
                            <td bgcolor=\"#ffffff\" align=\"left\">
                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                            <tr>
                            <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 20px 30px 10px 30px;\">
                            </td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                            <!-- COPY CALLOUT -->
                            <tr>
                            <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">
                            
                            </td>
                            </tr>
                            <!-- SUPPORT CALLOUT -->
                            <tr>
                            <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 30px 10px 0px 10px;\">
                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\" >
                            <!-- HEADLINE -->
                            <tr>
                            <td bgcolor=\"#FFE0DE\" align=\"center\" style=\"padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;\" >
                            <h2 style=\"font-size: 20px; font-weight: 400; color: #111111; margin: 0;\">Questions?</h2>
                            <p style=\"margin: 0;\"><a href=\"mailto:analytics@gpg.com?Subject=Analytics%20Alerts\" target=\"_blank\" style=\"color: #ec6d64;\">Email Us</a></p>
                            </td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                            <!-- FOOTER -->
                            <tr>
                            <td bgcolor=\"#f4f4f4\" align=\"center\" style=\"padding: 0px 10px 0px 10px;\">
               
               </td>
               </tr>
               </table>
               </body>
               </html>")



from_email <- "Analytics_Alerts@gloverparkgroup.onmicrosoft.com"
to_emails <- c("nolsen@gpg.com")
smtp_details <- list(host.name = "smtp.office365.com", port = 587, 
                     user.name = "Analytics_Alerts@gloverparkgroup.onmicrosoft.com", 
                     passwd = "AnALYtiCKS-EndGAME", tls = TRUE)

#SLACK EMAIL: "z4p5k3o1a3y0x0k5@gpg.slack.com"

send.mail(from = from_email,
          to = distro_list$Email,
          cc = c("egross@gpg.com","kackerman@gpg.com"),
          bcc = c("nolsen@gpg.com", "jtaylor@gpg.com"),
          subject = "Signify Health Competitors Alert",
          body = competitors_email,
          authenticate  = TRUE,
          smtp = smtp_details,
          send = TRUE,
          encoding = 'utf-8',
          debug = TRUE,
          html = T)
